﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_S3.Test
{
    class UtilidadesT1Test
    {

        [Test]
        public void NumToRomanoCaso01()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(5);

            Assert.AreEqual("V",resultado);

        }

        [Test]
        public void NumToRomanoCaso02()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(1000);

            Assert.AreEqual("M", resultado);

        }
        [Test]
        public void NumToRomanoCaso03()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(004);

            Assert.AreEqual("IV", resultado);

        }
        [Test]
        public void NumToRomanoCaso04()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(99999999);

            Assert.AreEqual("ERROR", resultado);

        }
        [Test]
        public void NumToRomanoCaso05()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(3);

            Assert.AreEqual("III", resultado);

        }
        [Test]
        public void NumToRomanoCaso06()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(0);

            Assert.AreEqual("ERROR", resultado);

        }
        [Test]
        public void NumToRomanoCaso07()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(020);

            Assert.AreEqual("XX", resultado);

        }
        [Test]
        public void NumToRomanoCaso08()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(999);

            Assert.AreEqual("CMXCIX", resultado);

        }
        [Test]
        public void NumToRomanoCaso09()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(2021);

            Assert.AreEqual("ERROR", resultado);

        }
        [Test]
        public void NumToRomanoCaso10()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(123);

            Assert.AreEqual("CXXIII", resultado);

        }
        [Test]
        public void NumToRomanoCaso11()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(0900);

            Assert.AreEqual("CM", resultado);

        }
        [Test]
        public void NumToRomanoCaso12()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(00000800);

            Assert.AreEqual("DCCC", resultado);

        }
        [Test]
        public void NumToRomanoCaso13()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(000000000000000000002);

            Assert.AreEqual("II", resultado);

        }
        [Test]
        public void NumToRomanoCaso14()
        {
            UtilidadesT1 util = new UtilidadesT1();

            var resultado = util.NumToRomano(101);

            Assert.AreEqual("CI", resultado);

        }


        //EJERCICIO 2

        [Test]
        public void GanadorYakenponCaso01()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Juan", Eleccion = "Tijera"};
            Jugador j2 = new Jugador() { Nombre = "Raul", Eleccion = "Piedra" };

            var ganador = util.GanadorYakenpo(j1,j2);

            Assert.AreEqual("Raul",ganador);

        }


        [Test]
        public void GanadorYakenponCaso02()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Sonia", Eleccion = "Tijera" };
            Jugador j2 = new Jugador() { Nombre = "Esther", Eleccion = "Tijera" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Empate", ganador);

        }

        [Test]
        public void GanadorYakenponCaso03()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Jose", Eleccion = "Papel" };
            Jugador j2 = new Jugador() { Nombre = "Lizbeth", Eleccion = "Tijera" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Lizbeth", ganador);

        }


        [Test]
        public void GanadorYakenponCaso04()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Arturo", Eleccion = "Piedra" };
            Jugador j2 = new Jugador() { Nombre = "Ana", Eleccion = "Papel" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Ana", ganador);

        }

        [Test]
        public void GanadorYakenponCaso05()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Jorge", Eleccion = " Papel" };
            Jugador j2 = new Jugador() { Nombre = "Lupe", Eleccion = "Papel" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Empate", ganador);

        }

        [Test]
        public void GanadorYakenponCaso06()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "LUIS", Eleccion = "TIJERA   " };
            Jugador j2 = new Jugador() { Nombre = "ESTRELLA", Eleccion = " PIEDRA" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("ESTRELLA", ganador);

        }
        [Test]
        public void GanadorYakenponCaso07()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "MIGUEL", Eleccion = "PIEDRA" };
            Jugador j2 = new Jugador() { Nombre = "ROSA", Eleccion = "PIEDRA" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Empate", ganador);

        }

        [Test]
        public void GanadorYakenponCaso08()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "MONSERRAT", Eleccion = "TiJeRa" };
            Jugador j2 = new Jugador() { Nombre = "FLOR", Eleccion = "pIeDrA" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("FLOR", ganador);

        }

        [Test]
        public void GanadorYakenponCaso09()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Anheli", Eleccion = "paPEL" };
            Jugador j2 = new Jugador() { Nombre = "Hans", Eleccion = "PApel" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Empate", ganador);

        }

        [Test]
        public void GanadorYakenponCaso10()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "MONSERRAT", Eleccion = "     pIE dra" };
            Jugador j2 = new Jugador() { Nombre = "FLOR", Eleccion = "          TIJera      " };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("MONSERRAT", ganador);

        }

        [Test]
        public void GanadorYakenponCaso11()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "MONSERRAT", Eleccion = "     pIEdra" };
            Jugador j2 = new Jugador() { Nombre = "FLOR", Eleccion = "          T  IJer a      " };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("MONSERRAT", ganador);

        }

        [Test]
        public void GanadorYakenponCaso12()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Lopez", Eleccion = "TIJERA" };
            Jugador j2 = new Jugador() { Nombre = "Genoba           ", Eleccion = "                piedra" };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Genoba", ganador);

        }

        [Test]
        public void GanadorYakenponCaso13()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "**ROSITA***", Eleccion = "     p a p e l  " };
            Jugador j2 = new Jugador() { Nombre = "--JUANCITO--", Eleccion = "      t i j e ra           " };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("--JUANCITO--", ganador);

        }


        [Test]
        public void GanadorYakenponCaso14()
        {
            UtilidadesT1 util = new UtilidadesT1();

            Jugador j1 = new Jugador() { Nombre = "Chavez", Eleccion = "    pi e dra  " };
            Jugador j2 = new Jugador() { Nombre = "Castro", Eleccion = "    p i e d r a       " };

            var ganador = util.GanadorYakenpo(j1, j2);

            Assert.AreEqual("Empate", ganador);

        }
    }
}
