﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_S3
{
    public class UtilidadesT1
    {

        public static List<string> romanos = new List<string>() { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
        public static List<int> numeros = new List<int>() { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
        public String NumToRomano(int numero)
        {
            String numeroRomano ="" ;
            if (numero <=1000 && numero >0)
            {
                while (numero > 0)
                {
                  
                    var indice = numeros.FindIndex(x => x <= numero);
              
                    numero -= numeros[indice];
              
                    numeroRomano += romanos[indice];
                }
                return numeroRomano;
            }
            else
            {

                return "ERROR";
            }
        
        }

        
        public String GanadorYakenpo(Jugador jugador1,Jugador jugador2)
        {

            String ganador = "";

            if (jugador1.Eleccion.Replace(" ","").ToLower().Equals("piedra") && jugador2.Eleccion.Replace(" ", "").ToLower().Equals("papel"))
            {
                ganador = jugador2.Nombre.Replace(" ", "");
            }
            if (jugador1.Eleccion.Replace(" ", "").ToLower().Equals("papel") && jugador2.Eleccion.Replace(" ", "").ToLower().Equals("piedra"))
            {
                ganador = jugador1.Nombre.Replace(" ", "");
            }

            if (jugador1.Eleccion.Replace(" ", "").ToLower().Equals("piedra") && jugador2.Eleccion.Replace(" ", "").ToLower().Equals("tijera"))
            {
                ganador = jugador1.Nombre.Replace(" ", "");
            }
            if (jugador1.Eleccion.Replace(" ", "").ToLower().Equals("tijera") && jugador2.Eleccion.Replace(" ", "").ToLower().Equals("piedra"))
            {
                ganador = jugador2.Nombre.Replace(" ", "");
            }


            if (jugador1.Eleccion.Replace(" ", "").ToLower().Equals("papel") && jugador2.Eleccion.Replace(" ", "").ToLower().Equals("tijera"))
            {
                ganador = jugador2.Nombre.Replace(" ", "");
            }

            if (jugador1.Eleccion.Replace(" ", "").ToLower().Equals("tijera") && jugador2.Eleccion.Replace(" ", "").ToLower().Equals("papel"))
            {
                ganador = jugador1.Nombre.Replace(" ", "");
            }

            if (jugador1.Eleccion.Replace(" ", "").ToLower().Equals(jugador2.Eleccion.Replace(" ", "").ToLower()) )
            {
                ganador = "Empate";
            }


            return ganador;
        }

    }
}
